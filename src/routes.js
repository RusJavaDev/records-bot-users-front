import {createRouter, createWebHistory} from "vue-router";
import HomePage from '@/pages/home.vue'
import AppointmentsPage from '@/pages/appointments.vue'
import ContactsPage from '@/pages/contacts.vue'

const routerHistory = createWebHistory();

const routers = createRouter({
    history: routerHistory,
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage,
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: ContactsPage,
        },
        {
            path: '/appointments',
            name: 'appointments',
            component: AppointmentsPage,
        },
    ]
})

export default routers