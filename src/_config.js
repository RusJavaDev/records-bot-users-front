export const links = [
    {
        title: "Home",
        alias: "home",
        url: `/`
    },
    {
        title: "Contacts",
        alias: "contacts",
        url: `/contacts`
    },
    {
        title: "MyAppointments",
        alias: "my_appointments",
        url: `/appointments`
    }
]