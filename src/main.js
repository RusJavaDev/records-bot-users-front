import { createApp } from 'vue'
import App from './App.vue'

import routers from "./routes";

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

//icons
import {aliases, mdi} from 'vuetify/iconsets/mdi-svg'
import {
    mdiHome,
    mdiCalendarClock,
    mdiCancel,
    mdiEmoticon
} from '@mdi/js'

//Yandex
import YmapPlugin from 'vue-yandex-maps'
const settings = {
    apiKey: process.env.VUE_APP_YANDEX_API_KEY,
    lang: 'ru_RU',
    coordorder: 'latlong',
    debug: false,
    version: '2.1'
}

const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi',
        aliases: {
            ...aliases,
            home: mdiHome,
            appointment: mdiCalendarClock,
            cancel: mdiCancel,
            profile: mdiEmoticon
        },
        sets: {
            mdi,
        },
    },
})
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'

const app = createApp(App);
app.use(routers)
app.use(vuetify)
app.use(YmapPlugin, settings)

app.mount('#app')
